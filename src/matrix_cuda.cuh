#include "read_msh.h"
#include <Eigen/Dense>
#include "interior_inverse.h"
#ifndef MATRIX_CUDA_H
#define MATRIX_CUDA_H

int run_function(void);
void duffy(const double *t, const int element, const mesh *mymesh, double *result);

void
element_load(double (*func)(double, double), mesh *mymesh, Eigen::ArrayXXd &xis, Eigen::ArrayXXd &omegas, int element,
             Eigen::ArrayXXd &fa, Eigen::ArrayXXd &step_0, Eigen::ArrayXXd &step_1);

void l2_projection(double (*func)(double, double), mesh *mymesh, Eigen::ArrayXXd &xis, Eigen::ArrayXXd &omegas,
                   Eigen::VectorXd &return_vector);

void mass_mult(const Eigen::VectorXd &x, mesh *mymesh, Eigen::ArrayXXd &xis, Eigen::ArrayXXd &omegas,
               Eigen::VectorXd &return_vector);

void mass_mult_b(double *x, mesh *mymesh, Eigen::ArrayXXd &xis, Eigen::ArrayXXd &omegas,
                 double *return_vector);

void mass_mult_b(double *x, mesh *mymesh, double *return_vector, mass_info *m_info);

void mass_mult_bt(double *x, mesh *mymesh, double *return_vector, mass_info *m_info);

void stiff_mult(double *x, mesh *mymesh, Eigen::ArrayXXd &xis, Eigen::ArrayXXd &omegas, double *return_vector);

void mass_pcg_noprecond(Eigen::VectorXd &b, mesh *mymesh, Eigen::ArrayXXd &xis, Eigen::ArrayXXd &omegas,
                        Eigen::VectorXd &x);

void mass_pcg_precond(Eigen::VectorXd &b, mesh *mymesh, Eigen::ArrayXXd &xis, Eigen::ArrayXXd &omegas,
                      Eigen::VectorXd &x, interior_infos *int_infos,
                      mass_info *m_info, edge_info *e_info);
#endif