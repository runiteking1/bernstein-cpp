//
// Created by marshall on 4/24/19.
//

#ifndef BERNSTEINCPP_INTERIOR_INVERSE_H
#define BERNSTEINCPP_INTERIOR_INVERSE_H

#include <Eigen/Dense>

using namespace Eigen;

struct interior_infos {
    int num_internal_dofs;
    ArrayXd scaling;
    ArrayXXd j2b;
    ArrayXXd q;
    int num_elements;
    int p;
};


struct mass_info {
    Eigen::ArrayXXd xis;
    Eigen::ArrayXXd omegas;
};

struct edge_info {
    Eigen::ArrayXd edge_diags; // Create an array corresponding to each dof on the edges
    Eigen::ArrayXd vertex_diags;             // and for each vertex
    Eigen::ArrayXXd algo_coefs;        // Coefficients for edge transformations
    Eigen::ArrayXXd cinv_coefs;
    double *worker;
    double *sub_worker;
    int edges;
    int edge_dofs;
};

void process_interior(const ArrayXd &x, const int num_internal_dofs, const int num_elements, const int p,
                      const ArrayXXd &j2b,
                      ArrayXXd &q, const ArrayXd &scaling, double *out);

void process_interior(const ArrayXd &x, double *out, interior_infos *info);

void jacobi22bernsteincoefficients(int n, ArrayXXd &gamma);

void nu_coefficients(int n, ArrayXd &nus);

int binom(int n, int k);
#endif //BERNSTEINCPP_INTERIOR_INVERSE_H
