//
// Created by marshall on 4/19/19.
//

#include <cstdio>
#include "helpers.h"

double **mem_alloc2D(unsigned N_D1, unsigned N_D2) {
    double **U;
    U = new double *[N_D1];
    U[0] = new double[N_D1 * N_D2];
    for (unsigned r = 1; r < N_D1; ++r)
        U[r] = U[0] + r * N_D2;
    return U;
}

void print_matrix(double **A, int n, int m) {
    printf("Printing Matrix :\n");
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++)
            printf("%g ", A[i][j]);
        printf("\n");
    }
    printf("\n");
}

void delete_mem2d(double **arr) {
    delete[] arr[0];  // remove the pool
    delete[] arr;     // remove the pointers
}
