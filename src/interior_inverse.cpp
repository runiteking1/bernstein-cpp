//
// Created by marshall on 4/24/19.
//

#include "interior_inverse.h"
#include <iostream>
#include <Eigen/Dense>
#include <bernstein_mesh.h>
#include <cmath>

using namespace Eigen;
using namespace std;

int binom(int n, int k) {
    int C[k + 1];
    memset(C, 0, sizeof(C));

    C[0] = 1;  // nC0 is 1

    for (int i = 1; i <= n; i++) {
        // Compute next row of pascal triangle using
        // the previous row
        for (int j = min(i, k); j > 0; j--)
            C[j] = C[j] + C[j - 1];
    }
    return C[k];
}

double nu22(int nu_k, int nu_n, int r) {
    double nuknr = ((nu_n - r + 1.0) * (nu_n - r + 2.0) / 2.0) * (pow((-1), nu_k)) * (
            binom(nu_n - r, nu_k) * binom(nu_n + r + 5, nu_k) * 2.0) / (
                           (nu_k + 1.0) * (nu_k + 2.0) * binom(nu_n, nu_k));
    return nuknr;
}

void jacobi22bernsteincoefficients(int n, ArrayXXd &gamma) {
    double alpha = 2.0;
    gamma(0, 0) = 1.0;
    for (int r = 0; r < n + 1; ++r) {
        for (int i = 0; i < r + 1; ++i) {
            gamma(r, i) = pow((-1.0), (r + i)) * binom(r + alpha, alpha + i) * (r + 1.0) * (r + 2) /
                          ((r + 2.0 - i) * (r + 1.0 - i));
        }
    }
}

void degree_raising_1d(double *c, int len_c) {
    double holder = c[0], holder_1;
    int n = len_c - 1;
    for (int j = 1; j < n + 1; ++j) {
        holder_1 = c[j];
        c[j] = (n + 1 - j) * holder_1 / (n + 1.0) + j * holder / (n + 1.0);
        holder = holder_1;
    }
    c[n + 1] = holder;
}

void degree_raising_2d(ArrayXXd &c, int m) {
    double holder;
    for (int k = m + 1; k > -1; --k) {
        for (int i = m + 1 - k; i > -1; --i) {
            holder = 0;
            if (i > 0) holder = holder + i * c(i - 1, k) / (m + 1.0);
            if (k > 0) holder = holder + k * c(i, k - 1) / (m + 1.0);
            if (m + 1 - k - i > 0) holder = holder + (m + 1 - k - i) * c(i, k) / (m + 1.0);

            c(i, k) = holder;
        }
    }
}

double norm_pnr222_square(int n, int r) {
    double h1 = 1.0 / (2.0 * r + 5.0) * ((r + 1.0) * (r + 2.0) / ((r + 3.0) * (r + 4.0)));
    double h2 = 1.0 / (2.0 * (n + 4.0)) * ((n - r + 1.0) * (n - r + 2.0) / ((n + r + 6.0) * (n + r + 7.0)));
    return h1 * h2;
}

void j22b2d(const double *f, const int n, const ArrayXXd &c,
            ArrayXXd &q, ArrayXd &b, double *worker, ArrayXXd &qn, ArrayXd &sn) {
    int j;


    // Step 1 (verified)
//    cout << "\nB: " <<  b << endl << endl;

    for (int r = 0; r < n + 1; ++r) {
        // Equivalent to "gamma = c[r, range(0, r + 1)]"
        for (int k = 0; k < r + 1; k++) {
            worker[k] = c(r, k);
        }

        for (int k = n - r; k > -1; --k) {
            for (int i = 0; i < n - k + 1; ++i) {
                j = n - k - i;
                qn(k, r) = qn(k, r) + worker[i] * f[i + (n + 1) * j];
            }
            sn[r] = sn[r] + qn(k, r) * nu22(k, n, r);

            degree_raising_1d(worker, n - k + 1); // worker array is correct
        }

        b[n + (n + 1) * r] = sn[r] / norm_pnr222_square(n, r);
    }
//    cout << "\nQn: " <<  qn<< endl << endl;
//    cout << "\nB: " <<  b << endl << endl;

    // Step 2 (verified)
    for (int i = n - 1; i > -1; --i) {
        // Reuse sn from above as Sm; need to reset to zero; only need (i+1) elements
        for (int r = 0; r < i + 1; ++r) {
            sn[r] = 0;
        }

        for (int r = 0; r < i + 1; ++r) {
            for (int k = 0; k < i - r + 1; ++k) {
                qn(k, r) = qn(k + 1, r) * (k + 1) / (i + 1) + qn(k, r) * (i + 1 - k) / (i + 1);
                sn[r] = sn[r] + qn(k, r) * nu22(k, i, r);

            }
            b[i + (n + 1) * r] = sn[r] / norm_pnr222_square(i, r);
        }
    }


    // Second stage
    for (int k = 0; k < n + 1; ++k) {
//        cout << "K " << k << endl;
        // Reuse sn from above (previously Sm) as Tk now; reset k + 1 elements
        for (int r = 0; r < k + 1; ++r) {
            sn[r] = 0;
        }

        // worker is standin for gamma_m (now needs two dimensional)
        for (int r = 0; r < k + 1; ++r) {
            worker[r + (n + 2) * k] = c(k, r);
        }

        // Step 1 (verified)
        for (int r = 0; r < k + 1; ++r) {
            // Tk = Tk + nu22(0, m, r) * gamma_r * b[m, r]
            // TODO: Try using daxpy eventually
            for (int i = 0; i < k + 1; ++i) {
                sn[i] = sn[i] + nu22(0, k, r) * b[k + (n + 1) * r] * worker[i + (n + 2) * r];

            }
            // Degree raise
            degree_raising_1d(&worker[(n + 2) * r], k + 1);
        }

//        cout << sn << endl;

        for (int i = 0; i < k + 1; ++i) {
            j = k - i;

            // Reusing qn from above (previously Qm, Qmp1); standin for cp
            qn(i, j) = sn[i];
        }

        // Step 2; compute T_{ijk}^m recursively
        if (k > 0) {
            for (int r = 0; r < k; ++r) {
                for (int i = 0; i < k - (r + 1) + 1; ++i) {
                    j = k - (r + 1) - i;
                    sn[i] = -((2 + i + 1) * sn[i + 1] / (2 + r + 1) + (2 + j + 1) * sn[i] / (2 + r + 1));
                    qn(i, j) = sn[i];
                }
            }
        }
//        cout << "\nq: "<<q << endl;
        for (int i = 0; i < k + 1; ++i) {
            for (int j = 0; j < i + 1; ++j) {
                q(j, i - j) = q(j, i - j) + qn(j, i - j);
            }
        }
        if (k < n) {
            degree_raising_2d(q, k);
        }
    }
}

void bernstein_mass_solver(const double *f, const int n, const ArrayXXd &j22bc, ArrayXXd &q, double *output, ArrayXd &b,
                           double *worker, ArrayXXd qn, ArrayXd sn) {

    int ni = n - 3;
    int k, counter = 0;

    // Should pass in sooner or later
    double *ftilde;
    ftilde = (double *) malloc((ni + 1) * (ni + 1) * sizeof(double));

    for (int i = 0; i < ni + 1; ++i) {
        for (int j = 0; j < ni - i + 1; ++j) {
            k = ni - i - j;
            ftilde[i + (ni + 1) * j] = (f[counter] * (i + 1) * (j + 1) * (k + 1)) / (n * (n - 1) * (n - 2));
//            ftilde[j + (ni + 1) * i] = (f[counter] * (i + 1) * (j + 1) * (k + 1)) / (n * (n - 1) * (n - 2));
            counter = counter + 1;
        }
    }


    // Computation of BB-form of polynomial ptilde
    j22b2d(ftilde, ni, j22bc, q, b, worker, qn, sn);

    counter = 0;
    for (int i = 0; i < ni + 1; ++i) {
        for (int j = 0; j < ni - i + 1; ++j) {
            k = ni - i - j;
            output[counter] = ((i + 1) * (j + 1) * (k + 1)) * q(i, j) / (n * (n - 1) * (n - 2));

            counter += 1;
        }
    }

    // Free memory
    free(ftilde);
}

void process_interior(const ArrayXd &x, const int num_internal_dofs, const int num_elements, const int p,
                      const ArrayXXd &j2b,
                      ArrayXXd &q, const ArrayXd &scaling, double *out) {
    int counter = 0;
    ArrayXd b((p - 2) * (p - 2));
    double *worker = (double *) malloc((p - 1) * (p - 1) * sizeof(double));
    ArrayXXd qn(p - 2, p - 2);
    ArrayXd sn(p - 2);

    for (int i = 0; i < num_elements; ++i) {
        // Zero out q
        for (int j = 0; j < p - 2; ++j) {
            for (int k = 0; k < p - 2; ++k) {
                q(j, k) = 0;
            }
        }

        qn.setZero();
        sn.setZero();
        bernstein_mass_solver(&(x[counter]), p, j2b, q, &(out[counter]), b, worker, qn, sn);

        // Scale each term
        for (int j = 0; j < num_internal_dofs; ++j) {
            out[counter + j] /= scaling[i];
        }

        counter += num_internal_dofs;
    }

    free(worker);
}

void process_interior(const ArrayXd &x, double *out, interior_infos *info) {
    process_interior(x, info->num_internal_dofs, info->num_elements, info->p, info->j2b,
                     info->q, info->scaling, out);
}