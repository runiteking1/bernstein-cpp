//
// Created by marshall on 4/15/19.
//

#ifndef BERNSTEINCPP_READ_MSH_H
#define BERNSTEINCPP_READ_MSH_H

#include <cstdio>
#include <string>
#include <map>
#include <vector>

// Very simple mesh structure; we are not dealing with Dirichlet boundary conditions
struct mesh {
    double* vertices;
    int* elnode;
    int num_elements;
    int num_vertices;
    int p; // Order
    int q; // Quadrature rule
    int total_dofs; // Total numebr of dofs
    int dofs_per_element; // number of dofs per element
    int* eldof;
    double *areas;
    int num_edges;

    std::map<int, std::vector<int>> lookup_dof;
    std::map<std::vector<int>, int> lookup_multi;
};

void read_msh_file(std::string gmsh_filename, mesh* mymesh, bool verbose = false);

#endif //BERNSTEINCPP_READ_MSH_H
