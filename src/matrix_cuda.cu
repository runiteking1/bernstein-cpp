#include <iostream>
//#include "cudaHeaders.h"
#include "matrix_cuda.cuh"
#include "read_msh.h"
#include <Eigen/Dense>
#include <cmath>
#include <vector>
#include "interior_inverse.h"
#include "mass_preconditioner.h"
//#include <cublas_v2.h>

// Kernel function to add the elements of two arrays
//__global__
//void add(int n, float *x, float *y) {
//    int index = threadIdx.x;
//    int stride = blockDim.x;
//    for (int i = index; i < n; i += stride) {
//        printf("%d\n", threadIdx.x);
//        y[i] = x[i] + y[i];
//    }
//}

void duffy(const double *t, const int element, const mesh *mymesh, double *result) {
    /*
     * takes in t a 2d variable and x which describes a simplex and outputs result a 2d array which is the result of the duffy transform
     */
    const double x1 = mymesh->vertices[2 * mymesh->elnode[element * 3 + 0]];
    const double y1 = mymesh->vertices[2 * mymesh->elnode[element * 3 + 0] + 1];
    const double x2 = mymesh->vertices[2 * mymesh->elnode[element * 3 + 1] + 0];
    const double y2 = mymesh->vertices[2 * mymesh->elnode[element * 3 + 1] + 1];
    const double x3 = mymesh->vertices[2 * mymesh->elnode[element * 3 + 2] + 0];
    const double y3 = mymesh->vertices[2 * mymesh->elnode[element * 3 + 2] + 1];

    result[0] = t[0]*x1 + (t[1] * (1 - t[0]))*x2 + (1 - t[0] - (t[1] * (1 - t[0]))) * x3;
    result[1] = t[0]*y1 + (t[1] * (1 - t[0]))*y2 + (1 - t[0] - (t[1] * (1 - t[0]))) * y3;
}

//int run_function(void) {
//    int N = 1 << 20;
//    float *x, *y;
//
//    // Allocate Unified Memory – accessible from CPU or GPU
//    cudaMallocManaged(&x, N * sizeof(float));
//    cudaMallocManaged(&y, N * sizeof(float));
//
//    // initialize x and y arrays on the host
//    for (int i = 0; i < N; i++) {
//        x[i] = 1.0f;
//        y[i] = 2.0f;
//    }
//
//    // Run kernel on 1M elements on the GPU
//    add << < 1, 256 >> > (N, x, y);
//
//    // Wait for GPU to finish before accessing on host
//    cudaDeviceSynchronize();
//
//    // Check for errors (all values should be 3.0f)
//    float maxError = 0.0f;
//    for (int i = 0; i < N; i++)
//        maxError = fmax(maxError, fabs(y[i] - 3.0f));
//    std::cout << "Max error: " << maxError << std::endl;
//
//    // Free memory
//    cudaFree(x);
//    cudaFree(y);
//
//    return 0;
//}

void moment(Eigen::ArrayXXd &fa, int q, int n, const Eigen::ArrayXXd &xis, const Eigen::ArrayXXd &omegas,
            Eigen::ArrayXXd &step_0,
            Eigen::ArrayXXd& step_1);

void
element_load(double (*func)(double, double), mesh *mymesh, Eigen::ArrayXXd &xis, Eigen::ArrayXXd &omegas, int element,
             Eigen::ArrayXXd &fa, Eigen::ArrayXXd &step_0, Eigen::ArrayXXd &step_1) {
    double t[2];
    double coord[2];

    for (int i = 0; i < mymesh->q; ++i) {
        for (int j = 0; j < mymesh->q; ++j) {
            t[0] = xis(1,i);
            t[1] = xis(0,j);
            duffy(t, element, mymesh, coord);
            fa(i, j) = func(coord[0], coord[1]);
        }
    }

    moment(fa, mymesh->q, mymesh->p, xis, omegas, step_0, step_1);

    step_1 = 2 * mymesh->areas[element] * step_1;
}

void moment(Eigen::ArrayXXd &fa, int q, int n, const Eigen::ArrayXXd &xis, const Eigen::ArrayXXd &omegas,
            Eigen::ArrayXXd &step_0, Eigen::ArrayXXd &step_1) {
    int i_l, alpha_l;
    double xi, s, r, w, omega;

    int l = 1;
    for (i_l = 0; i_l < q; ++i_l) {
        xi = xis(2 - l, i_l);
        omega = omegas(2 - l, i_l);

        s = 1 - xi;
        r = xi / s;

        w = omega * (std::pow(s, n));
        for (alpha_l = 0; alpha_l < n + 1; ++alpha_l) {
            for (int i_list = 0; i_list < q; ++i_list) {
                step_0(alpha_l, i_list) += w * fa(i_l, i_list);
            }

            w *= r * (n - alpha_l) / (1.0 + alpha_l);
        }
    }

    l = 2;
    for (int i_l = 0; i_l < q; ++i_l) {
        xi = xis(2-l, i_l);
        omega = omegas(2-l, i_l);

        s = 1 - xi;
        r = xi / s;

        for (int alphas = 0; alphas < n + 1; ++alphas) {
            w = omega * (std::pow(s, n - alphas));

            for (int alpha_l = 0; alpha_l < n - alphas + 1; ++alpha_l) {
                step_1(alphas, alpha_l) += w * step_0(alphas, i_l);

                w *= r * (n - alphas - alpha_l) / (1.0 + alpha_l);
            }
        }
    }
}

void l2_projection(double (*func)(double, double), mesh *mymesh, Eigen::ArrayXXd &xis, Eigen::ArrayXXd &omegas,
                   Eigen::VectorXd &return_vector) {
    Eigen::ArrayXXd fa_element(mymesh->q, mymesh->q);
    Eigen::ArrayXXd step_0 = Eigen::ArrayXXd::Zero(mymesh->p + 1, mymesh->q);
    Eigen::ArrayXXd step_1 = Eigen::ArrayXXd::Zero(mymesh->p + 1, mymesh->p + 1);
    for (int element = 0; element < mymesh->num_elements; ++element) {
        element_load(func, mymesh, xis, omegas, element, fa_element, step_0, step_1);
        for (int i = 0; i < mymesh->dofs_per_element; ++i) {
            return_vector[mymesh->eldof[element * mymesh->dofs_per_element + i]] += step_1(mymesh->lookup_dof[i][0],
                                                                                           mymesh->lookup_dof[i][1]);
        }

        // Reset working arrays
        fa_element.setZero();
        step_0.setZero();
        step_1.setZero();
    }
}

void evaluate(Eigen::ArrayXXd &c0, int q, int n, Eigen::ArrayXXd &xis, Eigen::ArrayXXd &step1, Eigen::ArrayXXd &step2) {
    int i_l, alphas, alpha_l;
    double xi, s, r, w;

    int l = 2;
    for (i_l = 0; i_l < q; ++i_l) {
        xi = xis(2 - l, i_l);
        s = 1 - xi;
        r = xi / s;

        for (alphas = 0; alphas < n + 1; ++alphas) {
            w = std::pow(s, n - alphas);
            for (alpha_l = 0; alpha_l < n - alphas + 1; ++alpha_l) {
                step1(alphas, i_l) += w * c0(alphas, alpha_l);

                w *= r * (n - alphas - alpha_l) / (1.0 + alpha_l);
            }
        }
    }

    l = 1;
    for (i_l = 0; i_l < q; ++i_l) {
        xi = xis(2 - l, i_l);
        s = 1 - xi;
        r = xi / s;

        w = std::pow(s, n);

        for (alpha_l = 0; alpha_l < n + 1; ++alpha_l) {
            for (int i_list = 0; i_list < q; ++i_list) {
                step2(i_l, i_list) += w * step1(alpha_l, i_list);
            }

            w *= r * (n - alpha_l) / (1.0 + alpha_l);
        }
    }

}

void mass_mult(const Eigen::VectorXd &x, mesh *mymesh, Eigen::ArrayXXd &xis, Eigen::ArrayXXd &omegas,
               Eigen::VectorXd &return_vector) {
    Eigen::ArrayXXd ca_element = Eigen::ArrayXXd::Zero(mymesh->p + 1, mymesh->p + 1);
    Eigen::ArrayXXd step_0 = Eigen::ArrayXXd::Zero(mymesh->p + 1, mymesh->q);
    Eigen::ArrayXXd step_1 = Eigen::ArrayXXd::Zero(mymesh->q, mymesh->q);
    std::vector<int> temp(3);
    for (int element = 0; element < mymesh->num_elements; ++element) {
        // Need to put vector into form that I know how to work with...

        for (int i = 0; i < mymesh->p + 1; ++i) {
            for (int j = 0; j < mymesh->p + 1 - i; ++j) {
                temp[0] = i;
                temp[1] = j;
                temp[2] = mymesh->p - i - j;

                // Translates to first look up multi index to retrieve local dof, then go to global dof by eldof
                // which then can be used to find the appropriate vector
                ca_element(i, j) = x[mymesh->eldof[element * mymesh->dofs_per_element + mymesh->lookup_multi[temp]]];
            }
        }

        evaluate(ca_element, mymesh->q, mymesh->p, xis, step_0, step_1);
        ca_element.setZero();
        step_0.setZero();
        moment(step_1, mymesh->q, mymesh->p, xis, omegas, step_0, ca_element);
        ca_element = 2 * mymesh->areas[element] * ca_element;
        for (int i = 0; i < mymesh->dofs_per_element; ++i) {
            return_vector[mymesh->eldof[element * mymesh->dofs_per_element + i]] += ca_element(mymesh->lookup_dof[i][0],
                                                                                               mymesh->lookup_dof[i][1]);
        }

        // Reset working arrays
        step_0.setZero();
        step_1.setZero();
        ca_element.setZero();
    }
}

void mass_mult_b(double *x, mesh *mymesh, Eigen::ArrayXXd &xis, Eigen::ArrayXXd &omegas,
                 double *return_vector) {
    Eigen::ArrayXXd ca_element = Eigen::ArrayXXd::Zero(mymesh->p + 1, mymesh->p + 1);
    Eigen::ArrayXXd step_0 = Eigen::ArrayXXd::Zero(mymesh->p + 1, mymesh->q);
    Eigen::ArrayXXd step_1 = Eigen::ArrayXXd::Zero(mymesh->q, mymesh->q);
    std::vector<int> temp(3);

    for (int element = 0; element < mymesh->num_elements; ++element) {
        // Need to put vector into form that I know how to work with...

        for (int i = 1; i < mymesh->p - 1; ++i) {
            for (int j = 1; j < mymesh->p - i; ++j) {
                temp[0] = i;
                temp[1] = j;
                temp[2] = mymesh->p - i - j;
                // Translates to first look up multi index to retrieve local dof, then go to global dof by eldof
                // which then can be used to find the appropriate vector
                ca_element(i, j) = x[mymesh->eldof[element * mymesh->dofs_per_element + mymesh->lookup_multi[temp]]];
            }
        }
        evaluate(ca_element, mymesh->q, mymesh->p, xis, step_0, step_1);
        ca_element.setZero();
        step_0.setZero();
        moment(step_1, mymesh->q, mymesh->p, xis, omegas, step_0, ca_element);
        ca_element = 2 * mymesh->areas[element] * ca_element;
        for (int i = 0; i < mymesh->dofs_per_element - (mymesh->p - 2) * (mymesh->p - 1) / 2; ++i) {
            return_vector[mymesh->eldof[element * mymesh->dofs_per_element + i]] += ca_element(mymesh->lookup_dof[i][0],
                                                                                               mymesh->lookup_dof[i][1]);
        }

        // Reset working arrays
        step_0.setZero();
        step_1.setZero();
        ca_element.setZero();
    }
}

void mass_mult_b(double *x, mesh *mymesh, double *return_vector, mass_info *m_info) {
    mass_mult_b(x, mymesh, m_info->xis, m_info->omegas, return_vector);
}

void mass_mult_bt(double *x, mesh *mymesh, Eigen::ArrayXXd &xis, Eigen::ArrayXXd &omegas,
                  double *return_vector) {
    Eigen::ArrayXXd ca_element = Eigen::ArrayXXd::Zero(mymesh->p + 1, mymesh->p + 1);
    Eigen::ArrayXXd step_0 = Eigen::ArrayXXd::Zero(mymesh->p + 1, mymesh->q);
    Eigen::ArrayXXd step_1 = Eigen::ArrayXXd::Zero(mymesh->q, mymesh->q);
    std::vector<int> temp(3);

    for (int element = 0; element < mymesh->num_elements; ++element) {
        // Need to put vector into form that I know how to work with...

        for (int i = 0; i < mymesh->p + 1; ++i) {
            for (int j = 0; j < mymesh->p + 1 - i; ++j) {
                temp[0] = i;
                temp[1] = j;
                temp[2] = mymesh->p - i - j;
                // Translates to first look up multi index to retrieve local dof, then go to global dof by eldof
                // which then can be used to find the appropriate vector
                if (i == 0 || j == 0 || temp[2] == 0) {
                    ca_element(i, j) = x[mymesh->eldof[element * mymesh->dofs_per_element +
                                                       mymesh->lookup_multi[temp]]];
                }
            }
        }

        evaluate(ca_element, mymesh->q, mymesh->p, xis, step_0, step_1);
        ca_element.setZero();
        step_0.setZero();
        moment(step_1, mymesh->q, mymesh->p, xis, omegas, step_0, ca_element);
        ca_element = 2 * mymesh->areas[element] * ca_element;
        // Only need to update interior dofs
        for (int i = mymesh->dofs_per_element - (mymesh->p - 2) * (mymesh->p - 1) / 2;
             i < mymesh->dofs_per_element; ++i) {
            return_vector[mymesh->eldof[element * mymesh->dofs_per_element + i] \
 - mymesh->num_vertices - (mymesh->p - 1) * mymesh->num_edges] += ca_element(mymesh->lookup_dof[i][0],
                                                                                               mymesh->lookup_dof[i][1]);
        }

        // Reset working arrays
        step_0.setZero();
        step_1.setZero();
        ca_element.setZero();
    }
}

void mass_mult_bt(double *x, mesh *mymesh, double *return_vector, mass_info *m_info) {
    mass_mult_bt(x, mymesh, m_info->xis, m_info->omegas, return_vector);
}


void stiff_mult(double *x, mesh *mymesh, Eigen::ArrayXXd &xis, Eigen::ArrayXXd &omegas, double *return_vector) {
    Eigen::ArrayXXd ca_element = Eigen::ArrayXXd::Zero(mymesh->p + 1, mymesh->p + 1);
    Eigen::ArrayXXd ca_element_x = Eigen::ArrayXXd::Zero(mymesh->p, mymesh->p);
    Eigen::ArrayXXd ca_element_y = Eigen::ArrayXXd::Zero(mymesh->p, mymesh->p);

    Eigen::ArrayXXd step_0_x = Eigen::ArrayXXd::Zero(mymesh->p + 1, mymesh->q);
    Eigen::ArrayXXd step_1_x = Eigen::ArrayXXd::Zero(mymesh->q, mymesh->q);
    Eigen::ArrayXXd step_0_y = Eigen::ArrayXXd::Zero(mymesh->p + 1, mymesh->q);
    Eigen::ArrayXXd step_1_y = Eigen::ArrayXXd::Zero(mymesh->q, mymesh->q);

    Eigen::ArrayXXd output_element = Eigen::ArrayXXd::Zero(mymesh->p + 1, mymesh->p + 1);

    std::vector<int> temp(3);

    Eigen::Vector2d v0;
    Eigen::Vector2d v1;
    Eigen::Vector2d v2;
    Eigen::Vector2d t0;
    Eigen::Vector2d t1;
    Eigen::Vector2d t2;
    Eigen::Vector2d n0;
    Eigen::Vector2d n1;
    Eigen::Vector2d n2;

    for (int element = 0; element < mymesh->num_elements; ++element) {
        // Need to put vector into form that I know how to work with...
        for (int i = 0; i < mymesh->p + 1; ++i) {
            for (int j = 0; j < mymesh->p + 1 - i; ++j) {
                temp[0] = i;
                temp[1] = j;
                temp[2] = mymesh->p - i - j;

                // Translates to first look up multi index to retrieve local dof, then go to global dof by eldof
                // which then can be used to find the appropriate vector
                ca_element(i, j) = x[mymesh->eldof[element * mymesh->dofs_per_element + mymesh->lookup_multi[temp]]];
            }
        }

        // Find the tangents and normals
        v0[0] = mymesh->vertices[2 * mymesh->elnode[3 * element]];
        v0[1] = mymesh->vertices[2 * mymesh->elnode[3 * element] + 1];
        v1[0] = mymesh->vertices[2 * mymesh->elnode[3 * element + 1]];
        v1[1] = mymesh->vertices[2 * mymesh->elnode[3 * element + 1] + 1];
        v2[0] = mymesh->vertices[2 * mymesh->elnode[3 * element + 2]];
        v2[1] = mymesh->vertices[2 * mymesh->elnode[3 * element + 2] + 1];

        t0 = -(v0 - v1);
        t1 = -(v1 - v2);
        t2 = -(v2 - v0);

        n0 = {t0[1], -t0[0]};
        n0 = n0 / t0.norm();

        n1 = {t1[1], -t1[0]};
        n1 = n1 / t1.norm();

        n2 = {t2[1], -t2[0]};
        n2 = n2 / t2.norm();

        for (int alpha1 = 0; alpha1 < mymesh->p; ++alpha1) {
            for (int alpha2 = 0; alpha2 < mymesh->p - alpha1; ++alpha2) {
                ca_element_x(alpha1, alpha2) = -mymesh->p * (ca_element(alpha1, alpha2) * n0(0) * t0.norm() +
                                                             ca_element(alpha1 + 1, alpha2) * n1(0) * t1.norm() +
                                                             ca_element(alpha1, alpha2 + 1) * n2(0) * t2.norm()) /
                                               (2 * mymesh->areas[element]);
                ca_element_y(alpha1, alpha2) = -mymesh->p * (ca_element(alpha1, alpha2) * n0(1) * t0.norm() +
                                                             ca_element(alpha1 + 1, alpha2) * n1(1) * t1.norm() +
                                                             ca_element(alpha1, alpha2 + 1) * n2(1) * t2.norm()) /
                                               (2 * mymesh->areas[element]);
            }
        }

        // Reuse v's as
        v0 = -t0.norm() * n0 / (2 * mymesh->areas[element]);
        v1 = -t1.norm() * n1 / (2 * mymesh->areas[element]);
        v2 = -t2.norm() * n2 / (2 * mymesh->areas[element]);


        evaluate(ca_element_x, mymesh->q, mymesh->p - 1, xis, step_0_x, step_1_x);
        evaluate(ca_element_y, mymesh->q, mymesh->p - 1, xis, step_0_y, step_1_y);


        // Reuse array
        ca_element_x.setZero();
        ca_element_y.setZero();
        step_0_x.setZero();
        step_0_y.setZero();


        moment(step_1_x, mymesh->q, mymesh->p - 1, xis, omegas, step_0_x, ca_element_x);
        ca_element_x = 2 * mymesh->areas[element] * ca_element_x;

        moment(step_1_y, mymesh->q, mymesh->p - 1, xis, omegas, step_0_y, ca_element_y);
        ca_element_y = 2 * mymesh->areas[element] * ca_element_y;

        for (int alpha1 = 0; alpha1 < mymesh->p; ++alpha1) {
            for (int alpha2 = 0; alpha2 < mymesh->p - alpha1; ++alpha2) {
                output_element(alpha1 + 1, alpha2) +=
                        mymesh->p * (ca_element_x(alpha1, alpha2) * v1[0] + ca_element_y(alpha1, alpha2) * v1[1]);
                output_element(alpha1, alpha2 + 1) +=
                        mymesh->p * (ca_element_x(alpha1, alpha2) * v2[0] + ca_element_y(alpha1, alpha2) * v2[1]);
                output_element(alpha1, alpha2) +=
                        mymesh->p * (ca_element_x(alpha1, alpha2) * v0[0] + ca_element_y(alpha1, alpha2) * v0[1]);
            }
        }
        for (int i = 0; i < mymesh->dofs_per_element; ++i) {
            return_vector[mymesh->eldof[element * mymesh->dofs_per_element + i]] += output_element(
                    mymesh->lookup_dof[i][0], mymesh->lookup_dof[i][1]);
        }

        // Reset working arrays
        step_0_x.setZero();
        step_1_x.setZero();
        step_0_y.setZero();
        step_1_y.setZero();
        ca_element_x.setZero();
        ca_element_y.setZero();
        output_element.setZero();
    }
}

using namespace std;

void mass_pcg_noprecond(Eigen::VectorXd &b, mesh *mymesh, Eigen::ArrayXXd &xis, Eigen::ArrayXXd &omegas,
                        Eigen::VectorXd &x) {
    /*
     * PCG with no checks strictly for the mass matrix
     *
     * x is the initial guess also
     */

    Eigen::VectorXd h(x.size()), g(x.size()), d(x.size());
    double resid, delta0, delta1, tau, beta;
    int iteration = 0;

    // First perform
    mass_mult(x, mymesh, xis, omegas, g); // g = A*x
    g = g - b;
    h = g;

    d = -h;
    resid = g.norm();
    delta0 = g.dot(h);

    if (resid < 1e-10) {}
//    cout << resid << endl;
    while (iteration < 50) {
        h.setZero();
        mass_mult(d, mymesh, xis, omegas, h); // h = Ad
        tau = delta0 / d.dot(h);

        x = x + tau * d;
        g = g + tau * h;

        resid = g.norm();
//        cout << tau << endl;
        std::cout << iteration << " " << resid << "\n";
        if (resid < 1e-10) {
            break;
        }

        h = g;

        delta1 = g.dot(h);
        beta = delta1 / delta0;
        delta0 = delta1;

        d = -h + beta * d;

        iteration += 1;
    }

}

void mass_pcg_precond(Eigen::VectorXd &b, mesh *mymesh, Eigen::ArrayXXd &xis, Eigen::ArrayXXd &omegas,
                      Eigen::VectorXd &x, interior_infos *int_infos,
                      mass_info *m_info, edge_info *e_info) {
    /*
     * PCG with no checks strictly for the mass matrix
     *
     * x is the initial guess also
     */

    Eigen::VectorXd h(x.size()), g(x.size()), d(x.size());
    double resid, delta0, delta1, tau, beta;
    int iteration = 0;

    // First perform
    mass_mult(x, mymesh, xis, omegas, g); // g = A*x
    g = g - b;
    precondition(g, h, mymesh, int_infos, m_info, e_info);

    d = -h;
    resid = g.norm();
    delta0 = g.dot(h);

    if (resid < 1e-10) {}

    while (iteration < 100) {
        h.setZero();
        mass_mult(d, mymesh, xis, omegas, h); // h = Ad
        tau = delta0 / d.dot(h);

        x = x + tau * d;
        g = g + tau * h;

        resid = g.norm();
//        cout << tau << endl;
        std::cout << iteration << " " << resid << "\n";
        if (resid < 1e-10) {
            break;
        }

        precondition(g, h, mymesh, int_infos, m_info, e_info);


        delta1 = g.dot(h);
        beta = delta1 / delta0;
        delta0 = delta1;

        d = -h + beta * d;

        iteration += 1;
    }

}


