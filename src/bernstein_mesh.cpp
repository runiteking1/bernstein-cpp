//
// Created by marshall on 4/17/19.
//

#include "bernstein_mesh.h"
#include <set>
#include <map>
#include <numeric>
#include <vector>

using namespace std;

typedef pair<int, int> pairs;

void print_eldof(mesh *mymesh) {
    for (int i = 0; i < mymesh->num_elements; ++i) {
        cout << "Element " << i << ": ";
        for (int j = 0; j < mymesh->dofs_per_element; ++j) {
            cout << mymesh->eldof[i * mymesh->dofs_per_element + j] << " ";
        }
        cout << "\n";
    }
}

int pos2d(int i, int j, int p);

vector<int> _lookup_dof(int i, mesh *mymesh, map<int, vector<int>> &face_dof_map);

double area_triangle(double dX0, double dY0, double dX1, double dY1, double dX2, double dY2) {
    double dArea = ((dX1 - dX0) * (dY2 - dY0) - (dX2 - dX0) * (dY1 - dY0)) / 2.0;
    return (dArea > 0.0) ? dArea : -dArea;
}


void construct_elnode(mesh *mymesh, bool verbose) {
    // Finds the total number of edges
    set<pairs> edges;
    pairs edge;

    int dof = mymesh->num_vertices;
    map<pairs, vector<int>> edge_dofs;
    mymesh->eldof = new int[mymesh->dofs_per_element * mymesh->num_elements];
    mymesh->areas = new double[mymesh->num_elements]; // Stores the area of the elements; we're doing linear stiffness/mass matrices
    int dofs_in_face = (mymesh->p - 1) * (mymesh->p - 2) / 2;
    for (int element = 0; element < mymesh->num_elements; ++element) {
        // First few dofs of an element are the three vertices
        mymesh->eldof[element * mymesh->dofs_per_element + 0] = mymesh->elnode[element * 3 + 0];
        mymesh->eldof[element * mymesh->dofs_per_element + 1] = mymesh->elnode[element * 3 + 1];
        mymesh->eldof[element * mymesh->dofs_per_element + 2] = mymesh->elnode[element * 3 + 2];

        // Now we have to deal with the edges
        edge = make_pair(min(mymesh->elnode[element * 3 + 0], mymesh->elnode[element * 3 + 1]),
                         max(mymesh->elnode[element * 3 + 0], mymesh->elnode[element * 3 + 1]));

        // If our edge is in the map, then we have to look this up
        if (edges.find(edge) != edges.end()) {
            for (int j = 0; j < mymesh->p - 1; ++j) {
                mymesh->eldof[(element * mymesh->dofs_per_element + 3) + j] = edge_dofs[edge][j];
            }
        } else {
            // Keep track of the
            edges.insert(edge);

            // Insert dofs into map so that we can look them up latter
            edge_dofs.insert(make_pair(edge, vector<int>(mymesh->p - 1)));
            iota(edge_dofs[edge].begin(), edge_dofs[edge].end(), dof);

            // Insert dofs into eldof
            for (int j = 0; j < mymesh->p - 1; ++j) {
                mymesh->eldof[(element * mymesh->dofs_per_element + 3) + j] = edge_dofs[edge][j];
            }
            // Increment dofs
            dof += mymesh->p - 1;
        }

        edge = make_pair(min(mymesh->elnode[element * 3 + 0], mymesh->elnode[element * 3 + 2]),
                         max(mymesh->elnode[element * 3 + 0], mymesh->elnode[element * 3 + 2]));
        if (edges.find(edge) != edges.end()) {
            // Insert dofs into eldof
            for (int j = 0; j < mymesh->p - 1; ++j) {
                mymesh->eldof[(element * mymesh->dofs_per_element + 3 + mymesh->p - 1) + j] = edge_dofs[edge][j];
            }
        } else {
            edges.insert(edge);

            // Insert dofs into map so that we can look them up latter
            edge_dofs.insert(make_pair(edge, vector<int>(mymesh->p - 1)));
            iota(edge_dofs[edge].begin(), edge_dofs[edge].end(), dof);

            // Insert dofs into eldof
            for (int j = 0; j < mymesh->p - 1; ++j) {
                mymesh->eldof[(element * mymesh->dofs_per_element + 3 + mymesh->p - 1) + j] = edge_dofs[edge][j];
            }

            dof += mymesh->p - 1;
        }

        edge = make_pair(min(mymesh->elnode[element * 3 + 2], mymesh->elnode[element * 3 + 1]),
                         max(mymesh->elnode[element * 3 + 2], mymesh->elnode[element * 3 + 1]));
        if (edges.find(edge) != edges.end()) {
            for (int j = 0; j < mymesh->p - 1; ++j) {
                mymesh->eldof[(element * mymesh->dofs_per_element + 3 + 2 * (mymesh->p) - 2) + j] = edge_dofs[edge][j];
            }
        } else {
            edges.insert(edge);

            // Insert dofs into map so that we can look them up latter
            edge_dofs.insert(make_pair(edge, vector<int>(mymesh->p - 1)));
            iota(edge_dofs[edge].begin(), edge_dofs[edge].end(), dof);
            dof += mymesh->p - 1;

            // Insert dofs into eldof
            for (int j = 0; j < mymesh->p - 1; ++j) {
                mymesh->eldof[(element * mymesh->dofs_per_element + 3 + 2 * (mymesh->p) - 2) + j] = edge_dofs[edge][j];
            }
        }

        // We also want to save the area
        mymesh->areas[element] = area_triangle(mymesh->vertices[2 * mymesh->elnode[element * 3 + 0]],
                                               mymesh->vertices[2 * mymesh->elnode[element * 3 + 0] + 1],
                                               mymesh->vertices[2 * mymesh->elnode[element * 3 + 1] + 0],
                                               mymesh->vertices[2 * mymesh->elnode[element * 3 + 1] + 1],
                                               mymesh->vertices[2 * mymesh->elnode[element * 3 + 2] + 0],
                                               mymesh->vertices[2 * mymesh->elnode[element * 3 + 2] + 1]);

    }

    // Edges and vertex dofs are distributed; now we need to assign a few internal dofs
    for (int element = 0; element < mymesh->num_elements; ++element) {
        iota(&(mymesh->eldof[element * mymesh->dofs_per_element + 3 * (mymesh->p)]),
             &(mymesh->eldof[element * mymesh->dofs_per_element + 3 * (mymesh->p) + dofs_in_face]), dof);

        dof += dofs_in_face;
    }
    mymesh->total_dofs = dof;
    mymesh->num_edges = edges.size();

    if (verbose) {
        cout << "Number of total edges: " << edges.size() << endl;
        cout << "Total number of dofs:" << dof << endl;
        print_eldof(mymesh);
    }

    // Need to create a map for the face dofs
    map<int, vector<int>> face_dof_to_multi;
    for (int i = 0; i < mymesh->p - 2; ++i) {
        for (int j = 0; j < mymesh->p - 2 - i; ++j) {
            int k = mymesh->p - 3 - i - j;
            // cout << i << " " << j << " " << k <<" " << pos2d(i, j, mymesh->p - 3) << endl;
            face_dof_to_multi.insert(make_pair(pos2d(i, j, mymesh->p - 3), vector<int>{i + 1, j + 1, k + 1}));
        }
    }

//    map<int, vector<int>> lookup_dof;
//    map<vector<int>, int> lookup_multi;
    for (int i = 0; i < mymesh->dofs_per_element; ++i) {
//        cout << i << " " << _lookup_dof(i, mymesh, face_dof_to_multi)[0] <<" " <<  _lookup_dof(i, mymesh, face_dof_to_multi)[1] << " " <<  _lookup_dof(i, mymesh, face_dof_to_multi)[2] << endl;
        mymesh->lookup_dof.insert(make_pair(i, _lookup_dof(i, mymesh, face_dof_to_multi)));
        mymesh->lookup_multi.insert(make_pair(_lookup_dof(i, mymesh, face_dof_to_multi), i));
    }
}

void set_3tuple(int i, int j, int k, int tuple[3]) {
    tuple[0] = i; tuple[1] = j; tuple[2] = k;
}

int pos2d(int i, int j, int p) {
    return ((i - 2 * p - 3) * (-i)) / 2 + j;
}

vector<int> _lookup_dof(int i, mesh *mymesh, map<int, vector<int>> &face_dof_map) {
    int p = mymesh->p;
    vector<int> tuple(3);
    if (i == 0) {
        set_3tuple(p, 0, 0, tuple.data());
        return tuple;
    }
    i -= 1;
    if (i == 0) {
        set_3tuple(0, p, 0, tuple.data());
        return tuple;
    }
    i -= 1;
    if (i == 0) {
        set_3tuple(0, 0, p, tuple.data());
        return tuple;
    }
    i -= 1;
    if (i < p - 1) {
        set_3tuple(p - (i + 1), i + 1, 0, tuple.data());
        return tuple;
    }
    i -= p - 1;
    if (i < p - 1) {
        set_3tuple(p - (i + 1), 0, i + 1, tuple.data());
        return tuple;
    }
    i -= p - 1;
    if (i < p - 1) {
        set_3tuple(0, p - (i + 1), i + 1, tuple.data());
        return tuple;
    }
    i -= p - 1;
    // cout << i << endl;
    // cout << face_dof_map[i][0] << " " << face_dof_map[i][1] << " " << face_dof_map[i][2] << endl;
    set_3tuple(face_dof_map[i][0], face_dof_map[i][1], face_dof_map[i][2], tuple.data());
    return tuple;
}

