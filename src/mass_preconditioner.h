//
// Created by marshall on 4/26/19.
//

#ifndef BERNSTEINCPP_MASS_PRECONDITIONER_H
#define BERNSTEINCPP_MASS_PRECONDITIONER_H

#include <Eigen/Dense>
#include "read_msh.h"
#include "interior_inverse.h"
using namespace Eigen;

void
process_edge(int edges, int edge_dofs, ArrayXd &diags, double *x, double *subworker, double *worker,
             ArrayXXd &cinv_coefs,
             ArrayXXd &algo_coefs, double *output);

void prepare_preconds(mesh *mymesh, edge_info *e_info);

void precondition(const VectorXd &input, VectorXd &output, mesh *mymesh, interior_infos *int_infos, mass_info *m_info,
                  edge_info *e_info);

#endif //BERNSTEINCPP_MASS_PRECONDITIONER_H
