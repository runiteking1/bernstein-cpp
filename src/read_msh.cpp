//
// Created by marshall on 4/15/19.
//

#include <read_msh.h>
#include <iostream>

using namespace std;
void read_msh_file(const string gmsh_filename, mesh *mymesh, bool verbose) {
    // TODO: need to make sure Python file outputs the dofs in order
    if (verbose) {
        cout << "\nReading gmsh data from: " << gmsh_filename << endl;
    }

    FILE *file_in = fopen(gmsh_filename.c_str(), "r");

    if (file_in == nullptr) {
        cout << "File is not there. Please specify the correct file" << endl;
        std::exit(1);
    }

    char buf[256];
    fgets(buf, 128, file_in);
    sscanf(buf, "%d %d", &(mymesh->num_vertices), &(mymesh->num_elements));

    if (verbose) {
        cout << "Number of vertices: " << mymesh->num_vertices << " and elements: " << mymesh->num_elements << endl;
    }

    mymesh->vertices = new double[mymesh->num_vertices * 2];
    mymesh->elnode = new int[mymesh->num_elements * 3];

    for (int i = 0; i < mymesh->num_vertices; ++i) {
        fgets(buf,128,file_in);
        sscanf(buf,"%lf %lf\n",&(mymesh->vertices[2*i]) ,&(mymesh->vertices[2*i + 1]));
    }
    for (int i = 0; i < mymesh->num_elements; ++i) {
        fgets(buf,128,file_in);
        sscanf(buf,"%d %d %d\n",&(mymesh->elnode[3*i]) ,&(mymesh->elnode[3*i + 1]), &(mymesh->elnode[3*i + 2]));
    }

    fclose(file_in);

    if (verbose) {
        cout << "Finished reading file" << endl;
    }
}
