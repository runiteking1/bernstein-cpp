//
// Created by marshall on 2/8/19.
//

#include "area.h"

using namespace Eigen;

template <typename T>

double area(T t0, T t1, T t2) {
    double t0norm = t0.norm();
    double t1norm = t1.norm();
    double t2norm = t2.norm();
    double s = (t0norm + t1norm + t2norm) / 2;
    return sqrt(s * (s - t0norm) * (s - t1norm) * (s - t2norm));
}