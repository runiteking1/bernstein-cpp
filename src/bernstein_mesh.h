//
// Created by marshall on 4/17/19.
//

#include <read_msh.h>
#include <iostream>
#ifndef BERNSTEINCPP_BERNSTEIN_MESH_H
#define BERNSTEINCPP_BERNSTEIN_MESH_H

void construct_elnode(mesh *mymesh, bool verbose = true);

#endif //BERNSTEINCPP_BERNSTEIN_MESH_H
