//
// Created by marshall on 2/8/19.
//

#ifndef BERNSTEINCPP_AREA_H
#define BERNSTEINCPP_AREA_H

#include <Eigen/Dense>

template <typename T>
double area(T t0, T t1, T t2);

#endif //BERNSTEINCPP_AREA_H
