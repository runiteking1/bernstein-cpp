//
// Created by marshall on 4/26/19.
//

#include "mass_preconditioner.h"
#include <iostream>
#include <Eigen/Dense>
#include <bernstein_mesh.h>
#include <cmath>
#include "interior_inverse.h"
#include "matrix_cuda.cuh"

using namespace Eigen;
using namespace std;

double mu(int i, int p) {
    return 64.0 * (i + 1) * (i + 2) / ((2 * i + 5) * (i + 3) * (i + 4) * (p + i + 4) * (p - i - 1));
}

void prepare_preconds(mesh *mymesh, edge_info *e_info) {
    e_info->edge_diags = ArrayXd::Zero(
            mymesh->num_edges * (mymesh->p - 1)); // Create an array corresponding to each dof on the edges
    e_info->vertex_diags = ArrayXd::Zero(mymesh->num_vertices);             // and for each vertex
    e_info->algo_coefs = ArrayXXd::Zero(mymesh->p - 1, mymesh->p - 1);        // Coefficients for edge transformations
    e_info->cinv_coefs = ArrayXXd::Zero(mymesh->p - 1, mymesh->p - 1);        // Coefficients for edge transformations
    e_info->worker = new double[mymesh->p + 1];
    e_info->sub_worker = new double[mymesh->p + 1];
    e_info->edge_dofs = mymesh->p - 1;
    e_info->edges = mymesh->num_edges;

    for (int element = 0; element < mymesh->num_elements; ++element) {
        // Sum the vertices
        e_info->vertex_diags[mymesh->elnode[3 * element]] += 16.0 * mymesh->areas[element] / 2;
        e_info->vertex_diags[mymesh->elnode[3 * element + 1]] += 16.0 * mymesh->areas[element] / 2;
        e_info->vertex_diags[mymesh->elnode[3 * element + 2]] += 16.0 * mymesh->areas[element] / 2;

        // Create and place the indices for the edges
        for (int i = 0; i < mymesh->p - 1; ++i) {
            e_info->edge_diags[mymesh->eldof[element * mymesh->dofs_per_element + 3 + i] - mymesh->num_vertices] +=
                    mu(i, mymesh->p) * mymesh->areas[element] / 2;
            e_info->edge_diags[mymesh->eldof[element * mymesh->dofs_per_element + 3 + mymesh->p - 1 + i] -
                               mymesh->num_vertices] +=
                    mu(i, mymesh->p) * mymesh->areas[element] / 2;
            e_info->edge_diags[mymesh->eldof[element * mymesh->dofs_per_element + 3 + 2 * (mymesh->p - 1) + i] -
                               mymesh->num_vertices] +=
                    mu(i, mymesh->p) * mymesh->areas[element] / 2;
        }
    }

    for (int n = 0; n < mymesh->p - 1; ++n) {
        for (int i = 0; i < n + 1; ++i) {
            e_info->algo_coefs(n, i) =
                    pow(-1, n - i) * binom(n + 2, i + 2) * (n + 1) * (n + 2) / ((n - i + 1) * (n - i + 2));
        }
    }

    for (int i = mymesh->p - 2; i > -1; --i) {
        for (int j = 0; j < i + 1; ++j) {
            e_info->cinv_coefs(i, j) = pow(-1, i - j) * (j + 1) * binom(i + 2, j + 2) / (i - j + 2);
        }
    }
}

void cinv(const double *f, double *work, const ArrayXXd &coefs, double *output, int len_f) {
    int n = len_f + 1;
    for (int i = 0; i < n - 1; ++i) {
        work[i] = f[i];
        output[i] = 0;
    }

    for (int outer_counter = 0; outer_counter < n - 1; ++outer_counter) {
        int i = n - 2 - outer_counter;

        if (i != n - 2) {
            int local_n = i + 3;
            for (int inner_counter = 0; inner_counter < i + 3 - 2; ++inner_counter) {
                work[inner_counter] = work[inner_counter] * (local_n - 1.0 - inner_counter) / local_n +
                                      work[inner_counter + 1] * (inner_counter + 2) / local_n;
            }
        }

        for (int j = 0; j < i + 1; ++j) {
            output[i] += 4 * coefs(i, j) * work[j];
        }
    }
}

void algo(const double *w, double *work, ArrayXXd coefs, double *output, int len_f) {
    double holder, holder_1;
    int p = len_f + 1;
    for (int n = 0; n < len_f; ++n) {
        if (n != 0) {
            holder = 0;
            for (int inner_loop = 0; inner_loop < n + 2; ++inner_loop) {
                holder_1 = work[inner_loop];
                work[inner_loop] = ((n - inner_loop) * work[inner_loop]) / n + inner_loop * holder / n;
                holder = holder_1;
            }
        }

        for (int i = 0; i < n + 1; ++i) {
            work[i] += coefs(n, i) * w[n];
        }
    }

    for (int n = 0; n < p - 1; ++n) {
        output[n] = (n + 1) * work[n] / (p - 1);
        output[n] *= 4.0 * (p - 1 - n) / p;
    }
}

void
process_edge(int edges, int edge_dofs, ArrayXd &diags, double *x, double *subworker, double *worker,
             ArrayXXd &cinv_coefs,
             ArrayXXd &algo_coefs, double *output) {
    int counter = 0;

    for (int edge = 0; edge < edges; ++edge) {
        // Compute the transformation
        cinv(&(x[counter]), subworker, cinv_coefs, worker, edge_dofs);
//        cout << algo_coefs << endl;

        // Compute diagonals
        for (int interior = 0; interior < edge_dofs; ++interior) {
//            worker[interior] = worker[interior] * diags[counter + interior];
            worker[interior] = worker[interior] / diags[counter + interior];
        }

//        // Reset worker array
        for (int interior = 0; interior < edge_dofs + 1; ++interior) {
            subworker[interior] = 0;
        }
//
//        cout << subworker << endl;
//        cout << algo_coefs << endl;
        algo(worker, subworker, algo_coefs, &(output[counter]), edge_dofs);
//        algo(edge_dofs);
//        algo(worker, subworker, algo_coefs, output, edge_dofs);

        counter += edge_dofs;

    }
}

void process_edge(double *x, double *output, edge_info *e_info) {
    process_edge(e_info->edges, e_info->edge_dofs, e_info->edge_diags, x, e_info->sub_worker, e_info->worker,
                 e_info->cinv_coefs, e_info->algo_coefs, output);
}

void precondition(const VectorXd &input, VectorXd &output, mesh *mymesh, interior_infos *int_infos,
                  mass_info *m_info, edge_info *e_info) {

    int int_dofs = int((mymesh->p - 1) * (mymesh->p - 2) / 2);
    int boundary_length = mymesh->total_dofs - int_dofs * mymesh->num_elements;
    VectorXd temp_rhs(boundary_length);
    VectorXd temp_holder(mymesh->total_dofs);
    temp_rhs.setZero();

    process_interior(input.tail(int_dofs * mymesh->num_elements), &(output[boundary_length]), int_infos);
    mass_mult_b(&(output[0]), mymesh, temp_rhs.data(), m_info);
    temp_rhs = input.head(boundary_length) - temp_rhs;

    // Solve edges
    process_edge(&(temp_rhs[mymesh->num_vertices]), &(output[mymesh->num_vertices]), e_info);
    // Solve vertex
    for (int i = 0; i < mymesh->num_vertices; ++i) {
        output[i] = pow(mymesh->p, 3) / e_info->vertex_diags[i] * temp_rhs[i];
    }

//    // Now backsolve after
    temp_rhs.resize(mymesh->total_dofs);
    temp_rhs.setZero();
    mass_mult_bt(&(output[0]), mymesh, temp_rhs.data(), m_info);
    process_interior(temp_rhs.head(int_dofs * mymesh->num_elements), &(temp_holder[boundary_length]), int_infos);
    output = output - temp_holder;
//    cout << output << endl;

}