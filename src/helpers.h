//
// Created by marshall on 4/19/19.
//

#ifndef BERNSTEINCPP_HELPERS_H
#define BERNSTEINCPP_HELPERS_H

double **mem_alloc2D(unsigned N_D1, unsigned N_D2);

void print_matrix(double **A, int n, int m);

void delete_mem2d(double **arr);

#endif //BERNSTEINCPP_HELPERS_H
