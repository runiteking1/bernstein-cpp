#include <iostream>
#include <Eigen/Dense>
#include <bernstein_mesh.h>
#include "src/area.h"
#include "read_msh.h"
#include "jacobi_polynomial.h"
#include <vector>
#include "helpers.h"
#include "matrix_cuda.cuh"
#include <cmath>
#include "interior_inverse.h"
#include "mass_preconditioner.h"
#include "omp.h"

//using namespace Eigen;
using namespace std;

double initial(double x, double y) {
    return 1;
//    return 4 * atan(exp(x + 1 - 2/cosh(y + 7) - 2/cosh(y - 7)));
//    return pow((-x - y) / 2, 2)*pow((x + 1) / 2, 1) * pow((y + 1) / 2, 1) * (y-1)/2 * (x-1)/2;
}
int main()
{
    // First let us read from a mesh file
    mesh mymesh;
//    read_msh_file("../single_triangle.mesh", &mymesh, true);
    read_msh_file("../wave.mesh", &mymesh, true);

    omp_set_num_threads(4);
#pragma omp parallel
    {
#pragma omp single
        cout << "Running OpenMP Version with " << omp_get_num_threads() << " threads. " << endl;
    }
    mymesh.p = 16;
    mymesh.dofs_per_element = ((mymesh.p + 2) * (mymesh.p + 1))/2;

    // Need to assign dofs
    construct_elnode(&mymesh);

    // Find the nodes and weights of the quadrature points
    int q = mymesh.p + 2; mymesh.q = q;
    mass_info m_info;
    m_info.xis = Eigen::ArrayXXd(2, q);
    m_info.omegas = Eigen::ArrayXXd(2, q);
    vector<double> weights(q), nodes(q);
    double t1, t2;
    for (int i = 0; i < 2; ++i) {
        roots_sh_jacobi(q, i + 1, 1, nodes.data(), weights.data());
        for (int j = 0; j < q; ++j) {
            m_info.xis(i, j) = nodes[j];
            m_info.omegas(i, j) = weights[j];
        }
    }

    // Create constants for interior solves (decided not to do OOP)
    interior_infos int_infos;
    int_infos.j2b = ArrayXXd(mymesh.p - 1, mymesh.p - 1);
    int_infos.q = ArrayXXd(mymesh.p - 2, mymesh.p - 2);
    jacobi22bernsteincoefficients(mymesh.p - 2, int_infos.j2b);
    int_infos.scaling = ArrayXd(mymesh.num_elements);
    for (int i = 0; i < mymesh.num_elements; ++i) {
        int_infos.scaling[i] = mymesh.areas[i];
    }
    int_infos.scaling = int_infos.scaling * 2;
    int_infos.num_internal_dofs = int((mymesh.p - 1) * (mymesh.p - 2) / 2);
    int_infos.num_elements = mymesh.num_elements;
    int_infos.p = mymesh.p;
    edge_info e_info;
    prepare_preconds(&mymesh, &e_info);

    // Set initial condition
    Eigen::VectorXd rhs_init(mymesh.total_dofs);
    Eigen::VectorXd u(mymesh.total_dofs);
    Eigen::VectorXd up(mymesh.total_dofs); // u prime
    Eigen::VectorXd holder(mymesh.total_dofs);
    l2_projection(initial, &mymesh, m_info.xis, m_info.omegas, rhs_init);
    mass_pcg_precond(rhs_init, &mymesh, m_info.xis, m_info.omegas, u, &int_infos, &m_info, &e_info);
//    cout << u;

    // Clean up memory
    delete[] mymesh.eldof;
    delete[] mymesh.elnode;
    delete[] mymesh.vertices;
    delete[] mymesh.areas;

    return 0;
}